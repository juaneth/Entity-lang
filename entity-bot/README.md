# Entity-Bot
## This is a specially modified version of [Flask](https://github.com/flask-discord/Flask), the discord bot both me and [@Ohmeg](https://github.com/Ohmeg) used to work on

## If you actually want a bot to run in your server, its best to fork Flask then modify that instead of cloning this as this is specially modified just for our Support server
